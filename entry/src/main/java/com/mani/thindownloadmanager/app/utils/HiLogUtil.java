/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mani.thindownloadmanager.app.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * HiLogUtil
 */
public class HiLogUtil {
    private static final HiLogLabel LOG_LABEL = new HiLogLabel(0, 0, "ThinDownloadManager");

    /**
     * error
     * @param logString logString
     */
    public static void error(String logString) {
        HiLog.error(LOG_LABEL, logString);
    }

    /**
     * error
     * @param logLabel1 logLabel1
     * @param logString logString
     */
    public static void error(HiLogLabel logLabel1, String logString) {
        HiLog.error(logLabel1, logString);
    }

    /**
     * info
     * @param logString logString
     */
    public static void info(String logString) {
        HiLog.info(LOG_LABEL, logString);
    }

    /**
     * debug
     * @param logString logString
     */
    public static void debug(String logString) {
        HiLog.debug(LOG_LABEL, logString);
    }
}
