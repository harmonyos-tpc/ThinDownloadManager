package com.mani.thindownloadmanager.app;

import com.mani.thindownloadmanager.app.utils.HiLogUtil;
import com.thin.downloadmanager.ThinDownloadManager;
import com.thin.downloadmanager.*;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.utils.net.Uri;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class RequestTest {

    @Test
    public void downloadPictureTest() {
        // 初始化下载器
        ThinDownloadManager downloadManager = new ThinDownloadManager(3);
        RetryPolicy retryPolicy = new DefaultRetryPolicy();
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        // 下载图片1
        Uri downloadUri2 = Uri.parse("https://httpbin.org/image/png");
        File filesDirs3 = new File(context.getCacheDir(), "/master3.png");
        Uri destinationUri2 = Uri.getUriFromFile(filesDirs3);
        DownloadRequest downloadRequest3 = new DownloadRequest(downloadUri2)
                .setDestinationURI(destinationUri2)
                .setPriority(DownloadRequest.Priority.LOW)
                .setRetryPolicy(retryPolicy)
                .setDownloadContext("Download3")
                .setStatusListener(new DownloadStatusListenerV1() {
                    @Override
                    public void onDownloadComplete(DownloadRequest downloadRequest) {
                        HiLogUtil.error("onDownloadComplete---完成下载-----" + "------" + filesDirs3.getTotalSpace());
                        Assert.assertTrue(filesDirs3.getTotalSpace() > 0);
                    }

                    @Override
                    public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                        HiLogUtil.error("onDownloadFailed---" + errorCode + "-----" + errorMessage + "-----");
                    }

                    @Override
                    public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {
                        HiLogUtil.error("onProgress---" + progress + "-----");

                    }
                });
        downloadManager.add(downloadRequest3);
        try {
            Thread.sleep(1000 * 20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void downloadFileTest() {
        // 初始化下载器
        ThinDownloadManager downloadManager = new ThinDownloadManager(3);
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Uri downloadUri4 = Uri.parse("https://httpbin.org/robots.txt");
        File filesDirs5 = new File(context.getCacheDir(), "/master5.txt");
        Uri destinationUri4 = Uri.getUriFromFile(filesDirs5);
        RetryPolicy retryPolicy = new DefaultRetryPolicy();
        DownloadRequest downloadRequest5 = new DownloadRequest(downloadUri4)
                .setDestinationURI(destinationUri4)
                .setPriority(DownloadRequest.Priority.HIGH)
                .setRetryPolicy(retryPolicy)
                .setDownloadContext("Download5")
                .setStatusListener(new DownloadStatusListenerV1() {
                    @Override
                    public void onDownloadComplete(DownloadRequest downloadRequest) {
                        HiLogUtil.error("onDownloadComplete---完成下载-----" + filesDirs5.getTotalSpace() + "------");
                        Assert.assertTrue(filesDirs5.getTotalSpace() > 0);
                    }

                    @Override
                    public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                        HiLogUtil.error("onDownloadFailed---" + errorCode + "-----" + errorMessage + "-----");
                    }

                    @Override
                    public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {
                        HiLogUtil.error("onProgress---" + progress + "-----");
                    }
                });

        downloadManager.add(downloadRequest5);
        try {
            Thread.sleep(1000 * 10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
