ThinDownloadManager
===================

Thin DownloadManager is an network library primary to download files

## 怎样编译？
```
1. 删除 config.json 中的 visible 属性配置
2. classpath版本1.0.8.+
```

## 集成配置 

### 方法一:
```
按需以module方式导入thindownloadmanager module到自己鸿蒙项目中；并使用下面的方式依赖
     implementation project(':thindownloadmanager')
```

### 方法二:
```
编译module生成har放进libs中
步骤：点击右侧gradle，选择对应的module；然后点击Tasks展开，然后点击other，再双击releaseHarmonyHar即生成Har；
     生成的har包在对应的module下面的build\outputs\har中
引入：将生成jar包放入对应entry或者module的libs目录中,并添加以下依赖配置
implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])
```

### 方法三
```
添加中心仓库
allprojects{
    repositories{
        mavenCentral()
    }
}
添加依赖配置
implementation 'io.openharmony.tpc.thirdlib:ThinDownloadManager:1.0.1' 
```

## 添加网络权限
```
  "reqPermissions": [
        {
          "name": "ohos.permission.INTERNET"
        }
      ]
```
## Usuage
#### **DownloadStatusListener (Deprecated)**
  * Provides call back option to know when the download is completed, failed and reason for failure, and to know the progress of the download.
``` java
    //Callback when download is successfully completed
    void onDownloadComplete (int id);

    //Callback if download is failed. Corresponding error code and
    //error messages are provided
    void onDownloadFailed (int id, int errorCode, String errorMessage);

    //Callback provides download progress
    void onProgress (int id, long totalBytes, long downlaodedBytes, int progress);

```
#### **DownloadStatusListenerV1**
  * Provides call back option to know when the download is completed, failed and reason for failure, and to know the progress of the download. DownloadRequest is given back in the callback so that you can easily set some Object as context to download request and get the context object back from the request object.
``` java
    //Callback when download is successfully completed
    void onDownloadComplete(DownloadRequest downloadRequest);

    //Callback if download is failed. Corresponding error code and
    //error messages are provided
    void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage);


    //Callback provides download progress
    void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress);

```

#### **DownloadRequest**
  * Takes all the necessary information required for download.
  * Download URI, Destination URI.
  * Set Priority for request as HIGH or MEDIUM or LOW.
  * Takes Callback listener DownloadStatusListener
  * Use custom Http Headers for a download request
  * Resumable a download if network connection drops or download is paused.
  * You can set a Retry Policy

     ``` java
        Uri downloadUri = Uri.parse("http://tcrn.ch/Yu1Ooo1");
        Uri destinationUri = Uri.parse(this.getExternalCacheDir().toString()+"/test.mp4");
        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .addCustomHeader("Auth-Token", "YourTokenApiKey")
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadContext(downloadContextObject)//Optional
                .setDownloadListener(new DownloadStatusListener() {
                    @Override
                    public void onDownloadComplete(int id) {

                    }

                    @Override
                    public void onDownloadFailed(int id, int errorCode, String errorMessage) {

                    }

                    @Override
                    public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress)) {

                    }
                });

     ```

#### **ThinDownloadManager**
  * The number of threads used to perform parallel download is determined by the available processors on the device. Uses `Runtime.getRuntime().availableProcessors()` api.
  
  	``` java
    private ThinDownloadManager downloadManager;
    .....
    
    downloadManager = new ThinDownloadManager();
    
    .... 
    ```

  * To start a download use *add( DownloadRequest request)*
   	```
   	int downloadId = downloadManager.add(downloadRequest);
   	```

  * To cancel a particular download use *cancel(int downloadId)* by passing download id.
  	- Returns 1 if successfull cancelled.
  	- Returns -1 if supplied download id is not found.

  	```
  	int status = downloadManager.cancel(downloadId);
  	```

  * To cancel all running requests use *cancelAll()*
  	```
  	downloadManager.cancelAll();
  	```

  * To query for a particular download use *query(int downloadId)*

    The possible status could be
  	- STATUS_PENDING
  	- STATUS_STARTED
  	- STATUS_RUNNING

  	```
  	int status = downloadManager.query(downloadId);
  	```
  * To pause a download in progress. The download request has to be marked as `setDownloadResumable` to true
    ``` java
    downloadManager.pause(downloadId)
    ```
  * To release all the resources used by download manager use *release()*.

  	```
  	downloadManager.release();
  	```

Make sure you included jcenter() in your repositories section.

## License
```
 Copyright 2013 Mani Selvaraj

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
```



