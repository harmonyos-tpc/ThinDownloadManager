package com.thin.downloadmanager.util;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public final class Log {
    private static String sTag = "ThinDownloadManager";
    private static boolean sEnabled = false;
    private static final HiLogLabel hiLogLabel = new HiLogLabel(0, 0, "ThinDownloadManager");

    private Log() {
    }

    public static boolean isEnabled() {
        return sEnabled;
    }

    public static void setEnabled(boolean enabled) {
        sEnabled = enabled;
    }

    public static boolean isLoggingEnabled() {
        return sEnabled;
    }

    public static int v(String msg) {
        if (sEnabled) {
            return HiLog.info(hiLogLabel, msg, sTag);
        }
        return 0;
    }

    public static int v(String tag, String msg) {
        if (sEnabled) {
            return HiLog.info(hiLogLabel, msg, sTag);
        }
        return 0;
    }

    public static int v(String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.info(hiLogLabel, msg, tr);
        }
        return 0;
    }

    public static int v(String tag, String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.info(hiLogLabel, msg, tr);
        }
        return 0;
    }

    public static int d(String msg) {
        if (sEnabled) {
            return HiLog.debug(hiLogLabel, msg);
        }
        return 0;
    }

    public static int d(String tag, String msg) {
        if (sEnabled) {
            return HiLog.debug(hiLogLabel, msg, tag);
        }
        return 0;
    }

    public static int d(String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.debug(hiLogLabel, msg, tr);
        }
        return 0;
    }

    public static int d(String tag, String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.debug(hiLogLabel, msg, tr);
        }
        return 0;
    }

    public static int i(String msg) {
        if (sEnabled) {
            return HiLog.info(hiLogLabel, msg);
        }
        return 0;
    }

    public static int i(String tag, String msg) {
        if (sEnabled) {
            return HiLog.info(hiLogLabel, msg);
        }
        return 0;
    }

    public static int i(String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.info(hiLogLabel, msg, tr);
        }
        return 0;
    }

    public static int i(String tag, String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.info(hiLogLabel, msg, tr);
        }
        return 0;
    }

    public static int w(String msg) {
        if (sEnabled) {
            return HiLog.warn(hiLogLabel, msg);
        }
        return 0;
    }

    public static int w(String tag, String msg) {
        if (sEnabled) {
            return HiLog.warn(hiLogLabel, msg);
        }
        return 0;
    }

    public static int w(String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.warn(hiLogLabel, msg);
        }
        return 0;
    }

    public static int w(String tag, String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.warn(hiLogLabel, msg, tr);
        }
        return 0;
    }

    public static int e(String msg) {
        if (sEnabled) {
            return HiLog.error(hiLogLabel, msg);
        }
        return 0;
    }

    public static int e(String tag, String msg) {
        if (sEnabled) {
            return HiLog.error(hiLogLabel, msg);
        }
        return 0;
    }

    public static int e(String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.error(hiLogLabel, msg, tr);
        }
        return 0;
    }

    public static int e(String tag, String msg, Throwable tr) {
        if (sEnabled) {
            return HiLog.error(hiLogLabel, msg, tr);
        }
        return 0;
    }

    public static int t(String msg, Object... args) {
        if (sEnabled) {
            return HiLog.debug(hiLogLabel, msg, args);
        }
        return 0;
    }
}